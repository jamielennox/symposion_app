symposion_app
===============

At this time, considerations have not been made to have the django project run
without additional infrastructure.

This can be configured in the future by switching the defualt
DEFAULT_FILE_STORAGE django default, and properly configuring django to load
subsequent configuration to switch this back to GCS on run in our testing
and production environments.

Login is a bit more complicated, the default flow redirects you out to a SAML
server, you can probably work something out with testshib.   But if you go to
/admin and log in, that will persist into /dashboard.


Required Configuration
----------------------

GCS
~~~

1. Set the bucket in settings.py
1. Store your Service Account JSON in a file
1. Set GOOGLE_APPLICATION_CREDENTIALS to your account file
1. Set GCS_BUCKET to your bucket name

SAML2
~~~~~

Please familurise yourself with Shibboleth configuraiton and the general
flow of how to register a SP with an IDP.

If you send the SP metadata staticly, validdity time must be removed.

You will also need to register your IDP metadata here either staticly or
remotely updated.

You may consider testing with testshib.

Configure signing and encryption keys, and configure them in the settings.py

VirtualEnv
~~~~~~~~~~

Not all things are lovely, so we use constraints to force the versions we
we wish for without having to do anything ugly.  This may require a newer
version of pip than is packaged with distros virtualenv

- ``virtualenv2 ve2``
- ``source ./ve2/bin/activate``
- ``pip install -c constraints.txt -r requirements.txt``

Pre-post-start configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that we are ready to start the app, we need to do initial setup, start
it, perform voodoo string magic, run more database jingling voodoo, and then
we can start it up and start looking around and customising the application.

``./manage.py migrate``
``./manage.py runserver``

Now we hit up the admin interface, find `symposion_proposals` `proposalkind`
and add `Talk` slug `talk`, `Tutorial`:`tutorial`, and `Miniconf`:`miniconf`.

Shut down and now run:
``./manage.py populate_inventory``

Now you can run the system and see how it goes.
